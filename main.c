/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/10 09:12:35 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/01/27 12:11:42 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"
#include <unistd.h>

int main(void)
{
	printf("printf returned num: %d\n", printf("%ld\n", -123));
	ft_printf("ft_printf returned num: %d\n", ft_printf("%ld\n", -123));
	return (0);
}
