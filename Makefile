# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/12/03 13:47:36 by ksarnyts          #+#    #+#              #
#    Updated: 2017/01/16 12:56:07 by ksarnyts         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

CFLAGS = -Wall -Wextra -Werror
SRC =	ft_atoi.c\
		ft_isdigit.c\
		ft_strlen.c\
		create_array.c\
		print_{decimal,specifier,unsigned,octal,hex,str,char,letter}.c\
		ft_put{char,nbr,str}.c \
		ft_print{hex,oct,f}.c

OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME):
	gcc $(CFLAGS) -c $(SRC)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all
